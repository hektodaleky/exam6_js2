/**
 * Created by Max on 23.12.2017.
 */
var maxTime = "1970-01-01";
function restartBlog() {
    maxTime = "1970-01-01";
    $('.posts').html("");
    getProfile();
}
function getProfile() {
    $.ajax({
        method: 'GET',
        url: 'http://146.185.154.90:8000/blog/maxgach@gmail.com/profile'
    }).then(function (result) {
        myFullName = result["firstName"] + " " + result['lastName'];
        myEmail = result["email"];
        $("#fullName").text(myFullName);
        getAllPosts(self.maxTime)

    }).catch(function () {
        alert("Неизвестная ошибка");
    })
};
function getAllPosts(time) {

    $.ajax({
        method: 'GET',
        url: "http://146.185.154.90:8000/blog/maxgach@gmail.com/posts?datetime=" + time


    }).then(function (result) {
        console.log(result);
        result.map(function (element) {
            getMessage(element);
            if (maxTime < element["datetime"])
                maxTime = element["datetime"];
        });

    }).catch(function (res) {
        console.log("Fail")
    });
}


function getMessage(onePost) {
    var oneDiv = $('<div class="one">');
    var auth = $('<p class="auth">').text(onePost["user"]["firstName"] + " " + onePost["user"]["lastName"] + " Said: ");
    var post = $('<p class="post">').text(onePost["message"]);
    $('.posts').prepend(oneDiv.append(auth).append(post));
}