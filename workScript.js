/**
 * Created by Max on 23.12.2017.
 */



var myFullName = "";
getProfile();


$('#edit-my-name').on('click', function () {
    console.log('click');
    $('.edit_profile').css('display', 'block');
    $('.black-overlay').css('display', 'block');
});

$('#my-name').on('click', function () {
    myFullName = $('#first').val() + " " + $("#second").val();
    $("#fullName").text(myFullName);

    $.ajax({
        method: 'POST',
        url: 'http://146.185.154.90:8000/blog/maxgach@gmail.com/profile',
        data: {firstName: $('#first').val(), lastName: $("#second").val()}
    }).then(
        function () {
            restartBlog();
            $('.edit_profile').css('display', 'none');
            $('.black-overlay').css('display', 'none');
        }
    ).catch(function () {
        alert("Ошибка при редактировании профиля");
    });
});
$('#open-email-window').on('click', function () {
    $('.edit_email').css('display', 'block');
    $('.black-overlay').css('display', 'block');
});

$('#add-email').on('click', function () {
    var innerEmail = $('#posts_email').val();
    if (innerEmail)
        $.ajax({
            method: 'POST',
            url: 'http://146.185.154.90:8000/blog/maxgach@gmail.com/subscribe',
            data: {email: innerEmail}
        }).then(function (res) {
            console.log(res);
            $('.edit_email').css('display', 'none');
            $('.black-overlay').css('display', 'none');
            restartBlog();
        }).catch(function () {
            alert("Ошибка при отправлении емайла на подписку");
        });



});


$('#send').on('click', function () {
    var tmpPost = $('#post-input').val();
    if (tmpPost) {
        $.ajax({
            method: 'POST',
            url: 'http://146.185.154.90:8000/blog/maxgach@gmail.com/posts',
            data: {message: tmpPost}
        }).then(function (res) {
            console.log(res);

            $('#post-input').val("");
        }).catch(function () {
            alert("Ошибка при получении данных");
        });
    }
    else
        alert("Сообщение не должно быть пустым");
});

var a = setInterval(function () {
    getAllPosts(self.maxTime)
}, 2000);
$('.close').on('click', function () {
    console.log($(this).parent().css('display', 'none'));
    $('.black-overlay').css('display', 'none');
});
